import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayConnection,
  OnGatewayInit,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway(80, {namespace: 'whiteboard'})
export class EventsGateway implements OnGatewayInit, OnGatewayConnection{
  
  // @WebSocketServer()
  private server: Server;
  
  afterInit(server: Server) {
    this.server = server;
    this.server.use((socket, next) => {
      const {token} = socket.handshake.query;
      
      //verify token here
      if(!token) {
        return next(new Error("not valid token"));
      }

      console.log('token :: ', token);
      next();
    });
  }


  handleConnection(socket: Socket) {
    const {participantId, meetingId} = socket.handshake.query;
    console.log("On connection : ", socket.nsp.name, socket.id, participantId, meetingId);
    socket.join(meetingId, () => {
      console.log(`clients in room '${meetingId}':`, socket.nsp.adapter.rooms[meetingId].length);
    });
  }

  @SubscribeMessage('drawing')
  drawing(socket: Socket, data: string) {
    const meetingId = socket.handshake.query.meetingId;
    console.log('meeting id:: ', meetingId);
    socket.broadcast.to(meetingId).emit('drawing', data);
  }
}